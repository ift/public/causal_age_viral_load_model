# Causal Age vs. Viral Load

Test of SARS-CoV-2 viral-load age-causal-dependence via a non-parametric, hierarchical, Bayesian, and causal model. The
following is a Python implementation of the model described in

* Guardiani et al.; Causal, Bayesian, & non-parametric modeling of the SARS-CoV-2 viral load distribution vs. patient’s age, PLoS ONE, https://doi.org/10.1371/journal.pone.0275011 [1].

Please cite [1] when making use of this code.

## Installation

The mandatory dependencies can be installed with

    sudo apt-get install git python3 python3-pip python3-dev
    pip3 install --user git+https://gitlab.mpcdf.mpg.de/ift/nifty.git@NIFTy_8

for a Debian-based distribution. Additionally, to plot results we require Matplotlib, which can be installed as follows

    sudo apt-get install python3-matplotlib python3-seaborn

For more information about the NIFTy package we refer to the [NIFTy 8 webpage](https://ift.pages.mpcdf.de/nifty/nifty8/index.html).

## Data Visualization

The unprocessed datasets are stored as `Cobas_Dataset.csv` and `LC_480_Dataset.csv`. The data can be visualized by
running the script `data.py` which outputs (for the two datasets respectively)

<img src="./plots/data_Cobas_Dataset.png" width="425"/> <img src="./plots/data_LC_480_Dataset.png" width="425"/>

These datasets have been extracted from Jones et
al. [preprint](https://virologie-ccm.charite.de/fileadmin/user_upload/microsites/m_cc05/virologie-ccm/dateien_upload/Weitere_Dateien/Charite_SARS-CoV-2_viral_load_2020-06-02.pdf) (Fig. 6).

## Run the code

The source code is located in the `src` directory.
The code can be run from the covid_combined_matern.py script. The necessary arguments that need to be parsed are a
config file (config.json) which contains the prior settings, the dataset (in .csv format) and the random seed for the
reshuffling of the viral load data (reshuffling_seed). If

* `reshuffling_seed = 0`, the viral-load data will not be reshuffled.
* `reshuffling_seed > 0`, the viral-load data wil be reshuffled (with random seed being `reshuffling_seed`).
* `reshuffling_seed < 0 and  != -3141592`, the age and-viral load axes will be switched throughout the analysis (
  $`X \leftrightarrow Y`$) and the data will be reshuffled with (with random seed being `- reshuffling_seed`).
* `reshuffling_seed = -3141592`, the age and viral-load axes will be switched, but the data will not be reshuffled.

For example, we can start the data analysis for the Cobas dataset without reshuffling via

    python3 covid_combined_matern.py --json_file ../config/config.json --csv_file ../Cobas_Dataset.csv --reshuffle_seed 0

Analogously, **after minimization**, the plots can be generated with

    python3 results_matern.py --json_file ../config/config.json --csv_file ../Cobas_Dataset.csv --reshuffle_seed 0

and the evidence calculated and stored in text files with the following

    python3 calculate_evidence.py --json_file ../config/config.json --csv_file ../Cobas_Dataset.csv --reshuffle_seed 0

### Input

As described in the above, the inputs of the code are (paths to) a prior configuration file (in .json format,
e.g. `config/config.json`), a file containing age and viral-load data (in .csv format, e.g. `Cobas_Dataset.csv`), and a
seed for reshuffling (which can also be used to invert the axes).

### Output

The output of the analysis can be divided into three categories.

1. `covid_combined_matern.py` will output a list of samples from the posterior distribution and some relevant plots
   produced during minimization to a specified folder (minimization can take up to one day per configuration).
2. `results_matern.py` will output a series of plots (an example of which is given below) to summarize the results.
These plots range from density reconstruction plots to infectivity-prediction plots.
3. `calculate_evidence`  will output evidence results into the specified folders to be used for model comparison.

<img src="./plots/results_reconstruction.png" width="425"/> <img src="./plots/results_conditional.png" width="425"/>
<img src="./plots/results_single.png" width="425"/> <img src="./plots/results_infectivity.png" width="425"/>

## LICENSE

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details. You should have received a copy of the GNU General Public License along with this
program. If not, see <http://www.gnu.org/licenses/>.

Copyright(C) 2013-2022 Max-Planck-Society.
