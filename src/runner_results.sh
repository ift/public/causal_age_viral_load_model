#!/usr/bin/bash
# Convenience shell script to run results on batch of different configurations

json_files=(../config/config.json ../config/config_filtered.json ../config/config_indep.json
../config/config_indep_filtered.json)
csv_files=(../Cobas_Dataset.csv ../LC_480_Dataset.csv)

for json_file in "${json_files[@]}"
do
	python results_matern.py --json_file $json_file --csv_file ${csv_files[0]} --reshuffle_seed 0
	python results_matern.py --json_file $json_file --csv_file ${csv_files[1]} --reshuffle_seed 0
    python results_matern.py --json_file $json_file --csv_file ${csv_files[0]} --reshuffle_seed 1
    python results_matern.py --json_file $json_file --csv_file ${csv_files[0]} --reshuffle_seed 2
    python results_matern.py --json_file $json_file --csv_file ${csv_files[0]} --reshuffle_seed 3
    python results_matern.py --json_file $json_file --csv_file ${csv_files[0]} --reshuffle_seed 4
    python results_matern.py --json_file $json_file --csv_file ${csv_files[0]} --reshuffle_seed 5
    python results_matern.py --json_file $json_file --csv_file ${csv_files[0]} --reshuffle_seed 6
    python results_matern.py --json_file $json_file --csv_file ${csv_files[0]} --reshuffle_seed 7
    python results_matern.py --json_file $json_file --csv_file ${csv_files[0]} --reshuffle_seed 8
    python results_matern.py --json_file $json_file --csv_file ${csv_files[0]} --reshuffle_seed 9
    python results_matern.py --json_file $json_file --csv_file ${csv_files[0]} --reshuffle_seed 10
done

