# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2013-2021 Max-Planck-Society
# Author: Matteo Guardiani
#
# NIFTy is being developed at the Max-Planck-Institut fuer Astrophysik.

import string

import nifty8 as ift
import numpy as np


class GeomMaskOperator(ift.LinearOperator):
    """
    Copyright @ Jakob Roth

    Takes a field and extracts the central part of the field corresponding to target.shape

    Parameters
    ----------
    domain : Domain, DomainTuple or tuple of Domain
        The operator's input domain.
    target : Domain, DomainTuple or tuple of Domain
        The operator's target domain
    """

    def __init__(self, domain, target):
        self._domain = ift.makeDomain(domain)
        self._target = ift.makeDomain(target)
        sl = []
        for i in range(len(self._domain.shape)):
            slStart = int((self._domain.shape[i] - self._target.shape[i]) / 2.)
            slStop = slStart + self._target.shape[i]
            sl.append(slice(slStart, slStop, 1))
        self._slices = tuple(sl)
        self._capability = self.TIMES | self.ADJOINT_TIMES

    def apply(self, x, mode):
        self._check_input(x, mode)
        x = x.val
        if mode == self.TIMES:
            res = x[self._slices]
            return ift.Field(self.target, res)
        res = np.zeros(self.domain.shape, x.dtype)
        res[self._slices] = x
        return ift.Field(self.domain, res)


class DomainBreak2D(ift.LinearOperator):
    def __init__(self, domain):
        self._domain = ift.DomainTuple.make(domain)
        self._target = ift.makeDomain((ift.RGSpace(self._domain.shape[0], self._domain[0].distances[0]),
                                       ift.RGSpace(self._domain.shape[1], self._domain[0].distances[1])))
        self._capability = self.TIMES | self.ADJOINT_TIMES

    def apply(self, x, mode):
        self._check_input(x, mode)
        val = x.val
        if mode == self.ADJOINT_TIMES:
            return ift.Field.from_raw(self._domain, val)
        else:
            return ift.Field.from_raw(self._target, val)


class out(ift.Operator):
    def __init__(self, domain):
        self._domain = ift.MultiDomain.make(domain)
        self._target = ift.DomainTuple.make((d for k in self._domain.keys() for d in self._domain[k]))
        self._l_key, self._r_key = tuple(self._domain.keys())

        tgt_reverse = ift.DomainTuple.make((d for k in tuple(self._domain.keys())[::-1] for d in self._domain[k]))
        alphabet = list(string.ascii_lowercase)
        off = len(self._domain[self._l_key])
        ss_dom_l = alphabet[:off][0]
        ss_dom_r = alphabet[off:off + len(self._domain[self._r_key])][0]
        ss = ss_dom_r + ss_dom_l + "->" + ss_dom_l + ss_dom_r
        self._T = ift.LinearEinsum(tgt_reverse, ift.MultiField.from_dict({}), ss)

    def apply(self, x):
        self._check_input(x)

        if isinstance(x, ift.Linearization):
            val = x.val.val
        else:
            val = x.val
        res = np.outer(val[self._l_key], val[self._r_key]).reshape(self._target.shape)

        if isinstance(x, ift.Linearization):
            jac_wrt_l = ift.OuterProduct(self._domain[self._l_key], x.val[self._r_key])
            jac_wrt_r = ift.OuterProduct(self._domain[self._r_key], x.val[self._l_key])

            jac = self._T @ jac_wrt_l.ducktape(self._l_key) + jac_wrt_r.ducktape(self._r_key)

            return x.new(ift.Field.from_raw(self._target, res), jac)
        else:
            return ift.Field.from_raw(self._target, res)


class y_averager(ift.LinearOperator):
    ''''
    Nifty linear Operator: computes the expectation value of a conditional probability distribution p(y|x),
    given as a field.
    Parameters:
    - RV (y, field)
    - dV volume element (float)
    returns:
    - ift.LinearOperator which computes the exp val 
    '''

    def __init__(self, domain, rv, dV, alpha):
        # FIX ME: Implement checks
        self._domain = ift.DomainTuple.make(domain)
        self._target = ift.makeDomain((ift.RGSpace(self._domain.shape[0], self._domain[0].distances[0])))
        self._rv = rv
        self._dV = dV
        self._alpha = alpha

        self._capability = self.TIMES

    def apply(self, x, mode):
        self._check_input(x, mode)
        val = x.val

        if self._alpha != 0:
            weights = np.power(10, self._rv.val * self._alpha) * self._dV
            res = np.sum((val * weights), axis=1)
            res = np.power(res, 1. / self._alpha)

        else:
            weights = self._rv.val * self._dV
            res = np.sum((val * weights), axis=1)

        if mode != self.TIMES:
            raise ValueError
        else:
            return ift.Field.from_raw(self._target, res)


class x_averager(ift.LinearOperator):
    ''''
    Nifty linear Operator: computes the expectation value of a conditional probability distribution p(x|y),
    given as a field.
    Parameters:
    - RV (x, field)
    - dV volume element (float)
    returns:
    - ift.LinearOperator which computes the exp val 
    '''

    def __init__(self, domain, rv, dV, alpha):
        # FIX ME: Implement checks
        self._domain = ift.DomainTuple.make(domain)
        self._target = ift.makeDomain((ift.RGSpace(self._domain.shape[1], self._domain[0].distances[1])))
        self._rv = rv
        self._dV = dV
        self._alpha = alpha

        self._capability = self.TIMES

    def apply(self, x, mode):
        self._check_input(x, mode)
        val = x.val

        if self._alpha is not 0:
            weights = np.power(10, self._rv.val * self._alpha) * self._dV
            res = np.sum((val * weights), axis=0)
            res = np.power(res, 1. / self._alpha)
            print(res.size)
            print(self._target)

        else:
            weights = self._rv.val * self._dV
            res = np.sum((val * weights), axis=0)

        if mode != self.TIMES:
            raise ValueError
        else:
            return ift.Field.from_raw(self._target, res)
