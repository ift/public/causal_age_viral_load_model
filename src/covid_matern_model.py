#!/usr/bin/env python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2013-2024 Max-Planck-Society
# Author: Matteo Guardiani
#
# NIFTy is being developed at the Max-Planck-Institut fuer Astrophysik.
import json
import os

import nifty8 as ift

import data
from data_utilities import fitted_infectivity
from utilities import DomainBreak2D, GeomMaskOperator
from utilities import y_averager


class MaternCausalModel:
    def __init__(self, setup, dataset):
        if not isinstance(setup, dict):
            raise TypeError("The setup argument needs to be of type dict.")

        if not isinstance(dataset, data.Data):
            raise TypeError("The dataset argument needs to be of type data.Data.")

        self.setup = setup
        self.dataset = dataset
        self.lambda_joint = None
        self.lambda_age = None
        self.lambda_ll = None
        self.lambda_age_full = None
        self.lambda_ll_full = None
        self.lambda_full = None
        self.amplitudes = None
        self.position_space = None
        self.target_space = None
        self.lambda_age_2d = None
        self.lambda_ll_2d = None
        self.lambda_combined, self.conditional_probability = self.create_model()
        print('Matérn kernel model initialized.')

    def create_model(self):
        self.lambda_joint, self.lambda_full = self.build_joint_component()

        self.lambda_age, self.lambda_ll, self.lambda_age_full, self.lambda_ll_full, self.amplitudes = \
            self.initialize_independent_components()

        # Dimensionality adjustment for the independent component
        self.target_space = self.lambda_joint.target

        domain_break_op = DomainBreak2D(self.target_space)

        lambda_joint_placeholder = ift.FieldAdapter(self.lambda_joint.target, 'lambdajoint')
        lambda_ll_placeholder = ift.FieldAdapter(self.lambda_ll.target, 'lambdall')
        lambda_age_placeholder = ift.FieldAdapter(self.lambda_age.target, 'lambdaage')

        age_marginalizer_op = domain_break_op(lambda_joint_placeholder.ptw('exp')).sum(
            0)  # Field exponentiation and marginalization along the age direction, hence has 'length' ll

        age_unit_field = ift.full(self.lambda_age.target, 1)
        dimensionality_operator = ift.OuterProduct(self.lambda_ll.target, age_unit_field)
        lambda_ll_2d = domain_break_op.adjoint @ dimensionality_operator @ lambda_ll_placeholder
        ll_unit_field = ift.full(self.lambda_ll.target, 1)
        dimensionality_operator_2 = ift.OuterProduct(self.lambda_age.target, ll_unit_field)
        transposition_operator = ift.LinearEinsum(dimensionality_operator_2(lambda_age_placeholder).target,
                                                  ift.MultiField.from_dict({}), "xy->yx")
        dimensionality_operator_2 = transposition_operator @ dimensionality_operator_2
        lambda_age_2d = domain_break_op.adjoint @ dimensionality_operator_2 @ lambda_age_placeholder

        joint_component = lambda_ll_2d + lambda_joint_placeholder
        cond_density = joint_component.ptw('exp') * domain_break_op.adjoint(
            dimensionality_operator(age_marginalizer_op.ptw('reciprocal')))
        normalization = domain_break_op(cond_density).sum(1)
        log_lambda_combined = lambda_age_2d + joint_component - domain_break_op.adjoint(
            dimensionality_operator(age_marginalizer_op.ptw('log'))) - domain_break_op.adjoint(
            dimensionality_operator_2(normalization.ptw('log')))

        log_lambda_combined = log_lambda_combined @ (
                self.lambda_joint.ducktape_left('lambdajoint') + self.lambda_ll.ducktape_left(
            'lambdall') + self.lambda_age.ducktape_left('lambdaage'))
        lambda_combined = log_lambda_combined.ptw('exp')

        conditional_probability = cond_density * domain_break_op.adjoint(dimensionality_operator_2(normalization)).ptw(
            'reciprocal')
        conditional_probability = conditional_probability @ (
                self.lambda_joint.ducktape_left('lambdajoint') + self.lambda_ll.ducktape_left('lambdall'))

        # Normalize the probability on the given logload interval
        boundaries = [min(self.dataset.coordinates()[0]), max(self.dataset.coordinates()[0]),
                      min(self.dataset.coordinates()[1]), max(self.dataset.coordinates()[1])]
        inv_norm = self.dataset.npix_ll / (boundaries[3] - boundaries[2])
        conditional_probability = conditional_probability * inv_norm
        self.lambda_age_2d = lambda_age_2d
        self.lambda_ll_2d = lambda_ll_2d

        return lambda_combined, conditional_probability

    def build_joint_component(self):
        npix_age = self.dataset.npix_age
        npix_ll = self.dataset.npix_ll
        self.position_space, sp1, sp2 = self.dataset.zero_pad()

        # Set up signal model
        joint_offset = self.setup['joint']['offset_dict']
        offset_mean = joint_offset['offset_mean']
        offset_std = joint_offset['offset_std']
        joint_prefix = joint_offset['prefix']

        joint_setup_ll = self.setup['joint']['log_load']
        ll_scale = joint_setup_ll['scale']
        ll_cutoff = joint_setup_ll['cutoff']
        ll_loglogslope = joint_setup_ll['loglogslope']
        ll_prefix = joint_setup_ll['prefix']

        joint_setup_age = self.setup['joint']['age']
        age_scale = joint_setup_age['scale']
        age_cutoff = joint_setup_age['cutoff']
        age_loglogslope = joint_setup_age['loglogslope']
        age_prefix = joint_setup_age['prefix']

        correlated_field_maker = ift.CorrelatedFieldMaker(joint_prefix)
        correlated_field_maker.set_amplitude_total_offset(offset_mean, offset_std)

        correlated_field_maker.add_fluctuations_matern(sp1, age_scale, age_cutoff, age_loglogslope, age_prefix)
        correlated_field_maker.add_fluctuations_matern(sp2, ll_scale, ll_cutoff, ll_loglogslope, ll_prefix)
        lambda_full = correlated_field_maker.finalize()

        # For the joint model unmasked regions
        tgt = ift.RGSpace((npix_age, npix_ll),
                          distances=(lambda_full.target[0].distances[0], lambda_full.target[1].distances[0]))

        GMO = GeomMaskOperator(lambda_full.target, tgt)
        lambda_joint = GMO(lambda_full.clip(-30, 30))

        full_domain_break_operator = DomainBreak2D(self.position_space)
        lambda_full = full_domain_break_operator.adjoint @ lambda_full

        return lambda_joint, lambda_full

    def build_independent_components(self, lambda_age_full, lambda_ll_full, amplitudes):
        # Split the center
        # age
        _dist = lambda_age_full.target[0].distances
        tgt_age = ift.RGSpace(self.dataset.npix_age, distances=_dist)
        GMO_age = GeomMaskOperator(lambda_age_full.target, tgt_age)
        lambda_age = GMO_age(lambda_age_full.clip(-30, 30))

        # Viral load
        _dist = lambda_ll_full.target[0].distances
        tgt_ll = ift.RGSpace(self.dataset.npix_ll, distances=_dist)
        GMO_ll = GeomMaskOperator(lambda_ll_full.target, tgt_ll)
        lambda_ll = GMO_ll(lambda_ll_full.clip(-30, 30))

        return lambda_age, lambda_ll, lambda_age_full, lambda_ll_full, amplitudes

    def initialize_independent_components(self):
        _, sp1, sp2 = self.dataset.zero_pad()

        # Set up signal model
        # age Parameters
        age_dictionary = self.setup['indep']['age']
        age_offset_mean = age_dictionary['offset_dict']['offset_mean']
        age_offset_std = age_dictionary['offset_dict']['offset_std']

        # Log Load Parameters
        ll_dictionary = self.setup['indep']['log_load']
        ll_offset_mean = ll_dictionary['offset_dict']['offset_mean']
        ll_offset_std = ll_dictionary['offset_dict']['offset_std']
        independent_ll_prefix = ll_dictionary['offset_dict']['prefix']

        # Create the age axis with the density estimator
        def remove_key(d, key):
            r = dict(d)
            del r[key]
            return r

        signal_response, operators = ift.density_estimator(sp1, cf_fluctuations=remove_key(age_dictionary['params'],
                                                                                           'prefix'),
                                                           cf_azm_uniform=age_offset_std, pad=0)
        lambda_age_full = operators["correlated_field"]
        age_amplitude = operators["normalized_amplitudes"][0]
        zero_mode = operators["amplitude_total_offset"]
        # response = ops["exposure"]

        correlated_field_maker = ift.CorrelatedFieldMaker(independent_ll_prefix)
        # Create the viral load axis with the Matérn-kernel correlated field
        correlated_field_maker.set_amplitude_total_offset(ll_offset_mean, ll_offset_std)
        correlated_field_maker.add_fluctuations_matern(sp2, **ll_dictionary['params'])
        lambda_ll_full = correlated_field_maker.finalize()
        ll_amplitude = correlated_field_maker.amplitude

        amplitudes = [age_amplitude, ll_amplitude]

        return self.build_independent_components(lambda_age_full, lambda_ll_full, amplitudes)

    def plot_prior_samples(self, n_samples):
        if not os.path.exists('../plots'):
            os.mkdir('../plots')
        plot = ift.Plot()

        for i in range(n_samples):
            mock_position = ift.from_random(self.lambda_combined.domain, 'normal')
            plot.add(self.lambda_combined(mock_position))

        filename = "plots/priors.pdf"
        plot.output(nx=int(n_samples / int(n_samples / 3)), ny=int(n_samples / 3), xsize=10, ysize=10, name=filename)

        print("Prior samples saved as", filename)

    def infectivity_analysis(self, alphas):
        # Utility operators
        domain_break = DomainBreak2D(self.target_space)
        age_unit_field = ift.full(self.lambda_age.target, 1)
        dimensionality_operator = ift.OuterProduct(self.lambda_ll.target, age_unit_field)
        ll_unit_field = ift.full(self.lambda_ll.target, 1)
        dimensionality_operator_2 = ift.OuterProduct(self.lambda_age.target, ll_unit_field)
        transposition_operator = ift.LinearEinsum(dimensionality_operator_2(self.lambda_age).target,
                                                  ift.MultiField.from_dict({}), "xy->yx")
        dimensionality_operator_2 = transposition_operator @ dimensionality_operator_2

        # Additional fields for plotting (indep)
        self.lambda_ll_2d = self.lambda_ll_2d @ self.lambda_ll.ducktape_left('lambdall')
        self.lambda_age_2d = self.lambda_age_2d @ self.lambda_age.ducktape_left('lambdaage')
        lambda_independent = self.lambda_age_2d + self.lambda_ll_2d
        lambda_independent = lambda_independent.exp()

        # True Joint
        margy = domain_break(self.lambda_joint.exp()).sum(1)
        margx = domain_break(self.lambda_joint.exp()).sum(0)

        lambda_true = domain_break.adjoint @ (dimensionality_operator(margx) + dimensionality_operator_2(margy))
        lambda_true = self.lambda_joint - lambda_true
        lambda_true = lambda_true.exp()

        # Averages
        averages = []
        boundaries = [min(self.dataset.coordinates()[0]), max(self.dataset.coordinates()[0]),
                      min(self.dataset.coordinates()[1]), max(self.dataset.coordinates()[1])]
        inv_norm = self.dataset.npix_ll / (boundaries[3] - boundaries[2])

        for alpha in alphas:
            globals()['y_average_%s' % alpha] = y_averager(self.conditional_probability.target,
                                                           ift.Field.from_raw(self.lambda_ll.target,
                                                                              self.dataset.coordinates()[1]),
                                                           1 / inv_norm,
                                                           alpha)
            globals()['load_average_%s' % alpha] = globals()['y_average_%s' % alpha](self.conditional_probability)
            averages.append(globals()['load_average_%s' % alpha])

        # Infectivity
        infectivity_levels = ['dw', 'mid', 'up']
        infectivity = []

        for it, inf in enumerate(infectivity_levels):
            globals()['infectivity_%s' % inf] = ift.Field.from_raw(self.lambda_ll.target, fitted_infectivity(
                self.dataset.coordinates()[1] + (it - 1)))
            globals()['infectivity_averager_%s' % inf] = y_averager(self.conditional_probability.target,
                                                                    globals()['infectivity_%s' % inf], 1 / inv_norm, 0)
            infectivity.append(globals()['infectivity_averager_%s' % inf](self.conditional_probability))

        return lambda_independent, lambda_true, averages, infectivity


if __name__ == '__main__':
    file_setup = open('../config/config.json', "r")
    setup = json.load(file_setup)
    file_setup.close()

    dataset = data.Data(128, 128, setup["threshold"], 0, "../Cobas_Dataset.csv")
    inverted_dataset = data.InvertedData(dataset)

    dataset.plot()
    inverted_dataset.plot()

    model = MaternCausalModel(setup, dataset)
    inverted_model = MaternCausalModel(setup, inverted_dataset)

    model.plot_prior_samples(8)
    model.infectivity_analysis(alphas=[0.75, 1., 2.])
