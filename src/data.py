#!/usr/bin/env python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2013-2024 Max-Planck-Society
# Author: Matteo Guardiani
#
import os
import sys

import nifty8 as ift
import numpy as np

from binner import bin_1d, bin_2d
from data_utilities import read_in


# define a new metaclass which overrides the "__call__" function
class ClassLoader(type):
    def __call__(cls, *args, **kwargs):
        obj = type.__call__(cls, *args, **kwargs)
        obj.__load__()
        return obj


class Data(metaclass=ClassLoader):
    """
    FIXME: write documentation

    """
    def __init__(self, npix_age, npix_ll, ll_threshold, reshuffle_seed, csv_dataset_path):
        if not isinstance(npix_age, int):
            raise TypeError("Number of pixels argument needs to be of type int.")

        if not isinstance(npix_ll, int):
            raise TypeError("Number of pixels argument needs to be of type int.")

        if not isinstance(ll_threshold, float):
            raise TypeError("Log load threshold value argument needs to be of type float.")

        if not isinstance(reshuffle_seed, int):
            raise TypeError("Reshuffle iterator argument needs to be of type int.")

        if not isinstance(csv_dataset_path, str):
            raise TypeError("The dataset file path argument needs to be of type str.")

        self.npix_age, self.npix_ll = npix_age, npix_ll
        self.ll_threshold = ll_threshold
        self.reshuffle_seed = reshuffle_seed
        self.csv_dataset_path = csv_dataset_path

        self.age, self.ll = self.filter()
        self.data = None
        self.edges = None
        self.filename = 'plots/data_'

    def __load__(self):
        self.data = self.__bin()[0]
        self.edges = [self.__bin()[1], self.__bin()[2]]

    def filter(self):
        # Loads, filters and reshuffles data
        self.age, self.ll = read_in(self.csv_dataset_path)
        self.ll, self.age = self.data_filter_x(self.ll_threshold, self.ll, self.age)

        if not self.reshuffle_seed == 0:
            self.__reshuffle_data(self.ll, self.reshuffle_seed)

        return self.age, self.ll

    def zero_pad(self):
        ext_npix_age = 2 * self.npix_age
        ext_npix_ll = 2 * self.npix_ll

        return self.__create_spaces(ext_npix_age, ext_npix_ll)

    def __bin(self):
        data, age_edges, ll_edges = bin_2d(self.age, self.ll, self.npix_age, self.npix_ll)
        self.data = np.array(data, dtype=np.int64)

        return data, age_edges, ll_edges

    def coordinates(self):
        age_coordinates = self.__obtain_coordinates(self.age, self.npix_age)
        ll_coordinates = self.__obtain_coordinates(self.ll, self.npix_ll)

        return age_coordinates, ll_coordinates

    def plot(self):
        import matplotlib.pyplot as plt
        self.filename += os.path.splitext(self.csv_dataset_path)[0] + ".png"
        if not os.path.exists('../plots'):
            os.mkdir('../plots')

        plt.imshow(self.data.T, origin='lower',
                   extent=[self.edges[0][0], self.edges[0][-1], self.edges[1][0], self.edges[1][-1]], aspect='auto')
        plt.colorbar()
        plt.tight_layout()
        plt.savefig(self.filename, dpi=200)
        plt.close()
        print("Dataset saved as", self.filename)

    @staticmethod
    def __create_spaces(npix_x, npix_y):
        position_space = ift.RGSpace((npix_x, npix_y))
        sp1 = ift.RGSpace(npix_x)
        sp2 = ift.RGSpace(npix_y)

        return position_space, sp1, sp2

    @staticmethod
    def __reshuffle_data(data, seed):
        from sklearn.utils import shuffle

        return shuffle(data, random_state=seed)

    @staticmethod
    def __obtain_coordinates(x, npix):
        binned_x, x_edges = bin_1d(x, npix)

        return 0.5 * (x_edges[:-1] + x_edges[1:])

    @staticmethod
    def data_filter_x(thresh, x, y):
        filtered_x = []
        filtered_y = []
        for i in range(x.shape[0]):
            if x[i] > thresh:
                filtered_x.append(x[i])
                filtered_y.append(y[i])
        print("Number of discarded datapoints: ", x.shape[0] - len(filtered_x), file=sys.stderr)
        return np.array(filtered_x), np.array(filtered_y)


class InvertedData(Data):
    def __init__(self, data):
        super().__init__(data.npix_age, data.npix_ll, data.ll_threshold, data.reshuffle_seed, data.csv_dataset_path)
        self.age, self.ll = self.ll, self.age
        self.npix_age, self.npix_ll = self.npix_ll, self.npix_age
        self.filename = 'plots/inverted_data.pdf'


if __name__ == "__main__":
    data = Data(90, 128, 0., 0, "../LC_480_Dataset.csv")
    data.plot()
    data = Data(90, 128, 0., 0, "../Cobas_Dataset.csv")
    data.plot()
