# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2013-2024 Max-Planck-Society
# Author: Matteo Guardiani
#
# NIFTy is being developed at the Max-Planck-Institut fuer Astrophysik.

import numpy as np


def bin_1d(data, N_bins, **kwargs):
    if not isinstance(data, np.ndarray):
        raise ValueError

    if len(data.shape) != 1:
        raise ValueError

    return np.histogram(data, bins=N_bins, **kwargs)


def bin_2d(data_x, data_y, nx_bins, ny_bins, **kwargs):
    if not isinstance(data_x, np.ndarray) or not isinstance(data_y, np.ndarray):
        raise ValueError

    if len(data_x.shape) != 1 or len(data_y.shape) != 1:
        raise ValueError

    return np.histogram2d(data_x, data_y, bins=[nx_bins, ny_bins], **kwargs)
