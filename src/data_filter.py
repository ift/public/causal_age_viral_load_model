import numpy as np

'''
This script serves as a test for the data-filtering function which is used in the covid_matern_model script.
'''


def data_filter_x(thresh, x, y):
    filtered_x = []
    filtered_y = []
    for i in range(x.shape[0]):
        if x[i] > thresh:
            filtered_x.append(x[i])
            filtered_y.append(y[i])
    print("Number of discarded datapoints: ", x.shape[0] - len(filtered_x), file=sys.stderr)
    return np.array(filtered_x), np.array(filtered_x)


if __name__ == "__main__":

    from data_utilities import read_in

    age, ll = read_in("../Cobas_Dataset.csv")
    ll, age = data_filter_x(0, ll, age)
