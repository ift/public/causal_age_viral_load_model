#!/usr/bin/env python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2013-2024 Max-Planck-Society
# Author: Matteo Guardiani
#
# NIFTy is being developed at the Max-Planck-Institut fuer Astrophysik.

import argparse
import json
import os

import matplotlib as mpl
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import nifty8 as ift

from const import npix_age, npix_ll
from covid_matern_model import MaternCausalModel
from data import Data, InvertedData
from data_utilities import fitted_infectivity
from utilities import DomainBreak2D

# Parser Setup
parser = argparse.ArgumentParser()
parser.add_argument('--json_file', type=str, required=True, help="File (.json) in which the priors are configured.")
parser.add_argument('--csv_file', type=str, required=True,
                    help="File (.csv) in which the age and viral load data is stored.")
parser.add_argument('--reshuffle_seed', type=int, required=True,
                    help="Integer representing the seed for data reshuffling.")

args = parser.parse_args()

json_file = args.json_file
csv_file = args.csv_file
reshuffle_iterator = args.reshuffle_seed


def fmt(x, pos):
    a, b = '{:.0e}'.format(x).split('e')
    b = int(b)
    return r'${} \times 10^{{{}}}$'.format(a, b)


def create_output_directory(config_file_path, data_file_path, reshuffling_seed):
    # Results Output Folders
    path_j = os.path.basename(config_file_path)
    path_t = os.path.basename(data_file_path)
    results_path = os.path.join('../covid_viral_load_results', os.path.splitext(path_j)[0], os.path.splitext(path_t)[0],
                                str(reshuffling_seed))
    results_path = os.path.normpath(results_path)

    save_results_path = os.path.join(results_path, 'results/')
    if not os.path.exists(save_results_path):
        os.makedirs(save_results_path)
    filename = save_results_path + 'results_{}.pdf'
    return results_path, filename


def main():
    """
    FIXME: Add docstring
    """

    # Read in the configuration file
    current_path = os.path.abspath('..')
    file_setup = open(json_file, "r")
    setup = json.load(file_setup)
    file_setup.close()

    # Load output directory
    results_path, filename = create_output_directory(json_file, csv_file, reshuffle_iterator)

    # Prepare the dataset
    data = Data(npix_age, npix_ll, setup['threshold'], abs(reshuffle_iterator), csv_file)
    if reshuffle_iterator < 0:
        if reshuffle_iterator == -3141592:
            data = Data(npix_age, npix_ll, setup['threshold'], 0, csv_file)
        data = InvertedData(data)

    # Load the model
    model = MaternCausalModel(setup, data)

    # Setup the response & define the amplitudes
    R = ift.GeometryRemover(model.lambda_combined.target)
    R_lamb = R(model.lambda_combined)
    A1 = model.amplitudes[0]
    A2 = model.amplitudes[1]

    # Specify data space
    data_space = R_lamb.target

    # Load posterior samples
    samples = ift.ResidualSampleList.load(results_path + "/pickle/last")
    n_samples = samples.n_samples
    # Utility operator to split domains
    domain_break = DomainBreak2D(model.target_space)

    # PLOTS: Settings
    ages = [10, 50, 80]  # Ages for which to show conditional probabilities
    if reshuffle_iterator < 0:
        # FIXME: check
        ages = [npix_ll // 4, npix_ll // 2, npix_ll // 4 * 3]
    alphas = [0]  # Values of alpha for which to calculate the averages
    # alphas = [1, 0.5, 0.25, 0]
    lambda_independent, lambda_true, averages, infectivity = model.infectivity_analysis(alphas)
    age_samples, ll_samples = [], []
    for sample in samples.iterator():
        age_samples.append(model.lambda_age_full.force(sample))  # FIXME: should these be evaluated at sample or at
        ll_samples.append(model.lambda_ll_full.force(sample))
        p1 = A1.force(sample)
        p2 = A2.force(sample)
    for age in ages:
        globals()['dtfi_%s' % age] = ift.DomainTupleFieldInserter(domain_break(model.conditional_probability).target, 0,
                                                                  (age,))
        globals()['cond_prob_%s' % age] = globals()[
                                              'dtfi_%s' % age].adjoint @ domain_break @ model.conditional_probability
    for age in ages:
        globals()['cond_prob_%s_mean' % age], globals()['cond_prob_%s_var' % age] = samples.sample_stat(
            globals()['cond_prob_%s' % age])

        for count, alpha in enumerate(alphas):
            globals()['load_avg_%s' % alpha] = averages[count]
            globals()['load_avg_sc_%s_mean' % alpha], globals()['load_avg_sc_%s_var' % alpha] = samples.sample_stat(
                globals()['load_avg_%s' % alpha])
    lambda_combined_mean, lambda_combined_var = samples.sample_stat(model.lambda_combined)
    conditional_probability_mean, conditional_probability_var = samples.sample_stat(model.conditional_probability)
    lambda_full_mean, lambda_full_var = samples.sample_stat(model.lambda_full)
    lambda_independent_mean, lambda_independent_var = samples.sample_stat(lambda_independent)
    sc5_m, sc5_var = samples.sample_stat(infectivity[0])
    sc6_m, sc6_var = samples.sample_stat(infectivity[1])
    sc7_m, sc7_var = samples.sample_stat(infectivity[2])

    # Paper Plots
    age_coordinates, ll_coordinates = data.coordinates()
    boundaries = [min(age_coordinates), max(age_coordinates), min(ll_coordinates), max(ll_coordinates)]
    filename_demo = filename.format('demo')
    import seaborn as sns
    nice_fonts = {'text.usetex': True, 'pgf.texsystem': 'pdflatex', 'axes.unicode_minus': False, 'font.family': 'serif',
                  'font.size': 11, 'axes.labelsize': 12, 'axes.titlesize': 11, 'xtick.labelsize': 11,
                  'ytick.labelsize': 11}
    mpl.rcParams.update(nice_fonts)
    plt.style.use('seaborn-paper')
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(15, 7))
    fig.suptitle('Paper Plots Demo')
    im = ax1.imshow(conditional_probability_mean.val.T, norm=colors.SymLogNorm(linthresh=8 * 10e-3), extent=boundaries,
                    origin="lower",
                    aspect='auto')
    clrs = sns.color_palette("husl", 5)
    ax1.set_title('Conditional Probability Reconstruction')
    ax1.set_xlabel(r'Age [yr]')
    ax1.set_ylabel(r'Viral load [$\log_{10}$(RNA copies)/mL]')
    if reshuffle_iterator < 0:
        ax1.set_ylabel(r'Age [yr]')
        ax1.set_xlabel(r'Viral load [$\log_{10}$(RNA copies)/mL]')

    # SINGLE PLOT p(y|age) 2D
    filename_conditional = filename.format('conditional')
    plt.figure(figsize=(5, 5))
    plt.rcParams.update(nice_fonts)
    # plt.title('Probability Distribution of Viral Load for Different Ages')
    plt.imshow(conditional_probability_mean.val.T, norm=colors.SymLogNorm(linthresh=8 * 10e-3), extent=boundaries,
               origin="lower",
               aspect='auto')
    # plt.grid(linestyle = '--')
    # plt.xlim(ll_coord[0], ll_coord[ll_coord.size-1])
    # plt.ylim()
    plt.xlabel(r'Age [yr]')
    plt.ylabel(r'Viral load [$\log_{10}$(RNA copies)/mL]')
    if reshuffle_iterator < 0:
        plt.ylabel(r'Age [yr]')
        plt.xlabel(r'Viral load [$\log_{10}$(RNA copies)/mL]')
    cbar = plt.colorbar()
    # cbar.yticklabels(ticklabs, fontsize=11)
    plt.tight_layout()
    plt.savefig(filename_conditional)
    plt.close()
    print("Saved results as '{}'.".format(filename_conditional))

    # DATA PLOT
    filename_data = filename.format('data')
    plt.figure(figsize=(5, 5))
    plt.rcParams.update(nice_fonts)
    # plt.title('Probability Distribution of Viral Load for Different Ages')
    plt.imshow(model.dataset.data.T, norm=colors.SymLogNorm(linthresh=10e-1), extent=boundaries, origin="lower",
               aspect='auto')
    plt.xlabel(r'Age [yr]')
    plt.ylabel(r'Viral load [$\log_{10}$(RNA copies)/mL]')
    if reshuffle_iterator < 0:
        plt.xlabel(r'Viral load [$\log_{10}$(RNA copies)/mL]')
        plt.ylabel(r'Age [yr]')
    cbar = plt.colorbar()
    # cbar.yticklabels(ticklabs, fontsize=11)
    plt.tight_layout()
    plt.savefig(filename_data)
    plt.close()
    print("Saved results as '{}'.".format(filename_data))
    # RECONSTRUCTION PLOT
    filename_reconstruction = filename.format('reconstruction')
    plt.figure(figsize=(5, 5))
    plt.rcParams.update(nice_fonts)
    # plt.title('Probability Distribution of Viral Load for Different Ages')
    plt.imshow(lambda_combined_mean.val.T, norm=colors.SymLogNorm(linthresh=10e-1), extent=boundaries, origin="lower",
               aspect='auto')
    plt.xlabel(r'Age [yr]')
    # plt.ylabel(r'p(viral load $|$ age, positive Cobas test) [ ]') # Check with T. if better or if should be put on
    # caption
    plt.ylabel(r'Viral load [$\log_{10}$(RNA copies)/mL]')
    if reshuffle_iterator < 0:
        plt.xlabel(r'Viral load [$\log_{10}$(RNA copies)/mL]')
        plt.ylabel(r'Age [yr]')
    cbar = plt.colorbar(format=ticker.FuncFormatter(fmt))
    # cbar.yticklabels(ticklabs, fontsize=11)
    plt.tight_layout()
    plt.savefig(filename_reconstruction)
    plt.close()
    print("Saved results as '{}'.".format(filename_reconstruction))

    # DEEPER CAUSAL ANALYSIS PLOTS
    for count, age in enumerate(ages):
        globals()['mean_%s' % age], globals()['std_%s' % age] = samples.sample_stat(globals()['cond_prob_%s' % age])
        globals()['mean_%s' % age], globals()['std_%s' % age] = globals()['mean_%s' % age].val, globals()[
            'std_%s' % age].sqrt().val

        ax2.plot(ll_coordinates, globals()['mean_%s' % age], c=clrs[count + 1], label=str(age) + " years-old")
        ax2.fill_between(ll_coordinates, globals()['mean_%s' % age] - globals()['std_%s' % age],
                         globals()['mean_%s' % age] + globals()['std_%s' % age], alpha=0.3,
                         facecolor=clrs[count + 1])
    ax2.grid(linestyle='--')
    ax2.set_title('Probability Distribution of the Viral Load for Different Ages')
    ax2.set(xlim=(ll_coordinates[0], ll_coordinates[-1]),
            ylim=(0, max(globals()['mean_%s' % min(ages)]) + globals()['std_%s' % min(ages)][0]))
    ax2.legend(prop={'size': 12})
    ax2.set_xlabel(r'Viral load [mL$^{-1}$]', fontsize=11)
    ax2.set_ylabel(r'p($\log_{10}$viral load)')
    if reshuffle_iterator < 0:
        ax2.set_title('Probability Distribution of the Age for Different Viral Loads')
        ax2.set(xlim=(ll_coordinates[0], ll_coordinates[-1]), ylim=(0, 0.05))
        ax2.legend(prop={'size': 12})
        ax2.set_xlabel(r'Age [yr]', fontsize=11)
        ax2.set_ylabel(r'p(Age)')
    # ax2.set_ylabel(r'Probability', fontsize=11)
    for item in ([ax1.title, ax1.xaxis.label, ax1.yaxis.label, ax2.title, ax2.xaxis.label,
                  ax2.yaxis.label] + ax1.get_xticklabels() + ax1.get_yticklabels() + ax2.get_xticklabels() +
                 ax2.get_yticklabels()):
        item.set_fontsize(11)
    cbar = fig.colorbar(im, ax=ax1)
    ticklabs = cbar.ax.get_yticklabels()
    cbar.ax.set_yticklabels(ticklabs, fontsize=11)
    # ax2.set_xscale('log', basex=10)
    plt.savefig(filename_demo)
    plt.close()
    print("Saved results as '{}'.".format(filename_demo))
    # SINGLE PLOT p(y|age) at fixed age
    filename_single = filename.format('single')
    plt.figure(figsize=(5, 5))
    plt.rcParams.update(nice_fonts)
    # plt.title('Probability Distribution of Viral Load for Different Ages')
    for count, age in enumerate(ages):
        globals()['mean_%s' % age], globals()['std_%s' % age] = samples.sample_stat(globals()['cond_prob_%s' % age])
        globals()['mean_%s' % age], globals()['std_%s' % age] = globals()['mean_%s' % age].val, globals()[
            'std_%s' % age].sqrt().val
        if reshuffle_iterator >= 0:
            plt.plot(ll_coordinates, globals()['mean_%s' % age], c=clrs[count + 1], label=str(age) + " years-old")
        else:
            plt.plot(ll_coordinates, globals()['mean_%s' % age], c=clrs[count + 1],
                     label=f"{age_coordinates[age]:.3}" + " [$\log_{10}$(RNA copies)/mL]")
        plt.fill_between(ll_coordinates, globals()['mean_%s' % age] - globals()['std_%s' % age],
                         globals()['mean_%s' % age] + globals()['std_%s' % age], alpha=0.3,
                         facecolor=clrs[count + 1])
        plt.fill_between(ll_coordinates, globals()['mean_%s' % age] - 2 * globals()['std_%s' % age],
                         globals()['mean_%s' % age] + 2 * globals()['std_%s' % age], alpha=0.25,
                         facecolor=clrs[count + 1])
    plt.grid(linestyle='--', dashes=(5, 4), linewidth=0.5)
    plt.xlim(ll_coordinates[0], ll_coordinates[-1])
    plt.ylim(0, max(globals()['mean_%s' % min(ages)]) + globals()['std_%s' % min(ages)][0])
    # plt.yscale('log', basey=10)
    # plt.xscale('log', basex=10)
    plt.legend(prop={'size': 12})
    plt.xlabel(r'Viral load [$\log_{10}$(RNA copies)/mL]')
    plt.ylabel(
        r'p($\log_{10}$viral load $|$ age, positive test)')  # Check with T. if better or if should be put on caption
    # plt.ylabel(r'Probability distribution [ ]')
    plt.ylim([0, 0.36])
    if reshuffle_iterator < 0:
        plt.xlabel(r'Age [yr]')
        plt.ylabel(r'p(age $|$ $\log_{10}$viral load, positive test)')  # Check with T. if better or if should be put on
        # caption
        # plt.ylabel(r'Probability distribution [ ]')
        plt.ylim(0, 0.035)
    plt.tight_layout()
    plt.savefig(filename_single)
    plt.close()
    print("Saved results as '{}'.".format(filename_single))
    for alpha in alphas:

        globals()['filename_avg_%s' % alpha] = filename.format('avg_' + str(alpha))
        plt.figure(figsize=(5, 5))
        plt.rcParams.update(nice_fonts)
        # plt.title('Average Viral Load')
        plt.plot(age_coordinates, globals()['load_avg_sc_%s_mean' % alpha].val)
        plt.fill_between(age_coordinates,
                         globals()['load_avg_sc_%s_mean' % alpha].val - globals()[
                             'load_avg_sc_%s_var' % alpha].sqrt().val,
                         globals()['load_avg_sc_%s_mean' % alpha].val + globals()[
                             'load_avg_sc_%s_var' % alpha].sqrt().val,
                         alpha=0.3)
        plt.fill_between(age_coordinates,
                         globals()['load_avg_sc_%s_mean' % alpha].val - 2 * globals()[
                             'load_avg_sc_%s_var' % alpha].sqrt().val,
                         globals()['load_avg_sc_%s_mean' % alpha].val + 2 * globals()[
                             'load_avg_sc_%s_var' % alpha].sqrt().val,
                         color='#1f77b4', alpha=0.2)
        plt.grid(linestyle='--')
        plt.xlim(age_coordinates[0], age_coordinates[age_coordinates.size - 1])

        plt.xlabel(r'Age [yr]')

        if alpha != 0:
            plt.yscale('log', base=10)
            plt.ylabel(r'Average viral load [copies/mL]')

        plt.ylabel(r'Average viral load [$\log_{10}$(RNA copies)/mL]')
        if reshuffle_iterator < 0:
            plt.xlabel(r'Viral load [$\log_{10}$(RNA copies)/mL]')
            plt.ylabel(r'Average age [yr]')

        plt.tight_layout()

        plt.savefig(globals()['filename_avg_%s' % alpha])
        plt.close()
        print("Saved results as '{}'.".format(globals()['filename_avg_%s' % alpha]))
    # INFECTIVITY AVERAGE
    filename_infect = filename.format('infectivity')
    plt.figure(figsize=(5, 5))
    plt.rcParams.update(nice_fonts)
    # plt.title('Average Viral Load')
    plt.plot(age_coordinates, sc7_m.val, color='#ff7f0e', label='Upper')
    plt.fill_between(age_coordinates, sc7_m.val - sc7_var.sqrt().val, sc7_m.val + sc7_var.sqrt().val, color='#ff7f0e',
                     alpha=0.3)
    plt.fill_between(age_coordinates, sc7_m.val - 2 * sc7_var.sqrt().val, sc7_m.val + 2 * sc7_var.sqrt().val,
                     color='#ff7f0e', alpha=0.2)
    plt.plot(age_coordinates, sc6_m.val, color='#1f77b4', label='Original')
    plt.fill_between(age_coordinates, sc6_m.val - sc6_var.sqrt().val, sc6_m.val + sc6_var.sqrt().val, color='#1f77b4',
                     alpha=0.3)
    plt.fill_between(age_coordinates, sc6_m.val - 2 * sc6_var.sqrt().val, sc6_m.val + 2 * sc6_var.sqrt().val,
                     color='#1f77b4', alpha=0.2)
    plt.plot(age_coordinates, sc5_m.val, color='#2ca02c', label='Lower')
    plt.fill_between(age_coordinates, sc5_m.val - sc5_var.sqrt().val, sc5_m.val + sc5_var.sqrt().val, color='#2ca02c',
                     alpha=0.3)
    plt.fill_between(age_coordinates, sc5_m.val - 2 * sc5_var.sqrt().val, sc5_m.val + 2 * sc5_var.sqrt().val,
                     color='#2ca02c', alpha=0.2)
    plt.grid(linestyle='--')
    plt.xlim(age_coordinates[0], age_coordinates[age_coordinates.size - 1])
    plt.xlabel(r'Age [yr]')
    plt.ylabel(r'Proportion of positive cultures $I(y)$')
    if reshuffle_iterator < 0:
        plt.title("This distribution is just an example. \nIt is not supported by any scientific finding.")
        plt.xlabel(r'Viral load [$\log_{10}$(RNA copies)/mL]')
        plt.ylabel(r'Proportion of positive cultures $I(y)$')
    plt.legend(prop={'size': 12})
    plt.tight_layout()
    plt.savefig(filename_infect)
    plt.close()
    print("Saved results as '{}'.".format(filename_infect))
    # NATURE PLOT
    filename_nature = filename.format('nature')
    plt.figure(figsize=(5, 5))
    plt.rcParams.update(nice_fonts)
    # plt.title('Average Viral Load')
    plt.plot(ll_coordinates, fitted_infectivity(ll_coordinates + 1), color='#ff7f0e', label='Upper')
    plt.plot(ll_coordinates, fitted_infectivity(ll_coordinates), color='#1f77b4', label='Middle')
    plt.plot(ll_coordinates, fitted_infectivity(ll_coordinates - 1), color='#2ca02c', label='Lower')
    plt.grid(linestyle='--')
    plt.xlim(ll_coordinates[0], ll_coordinates[-1])
    plt.xlabel(r'Viral load [$\log_{10}$(RNA copies)/mL]')
    plt.ylabel(r'Proportion of positive cultures')
    if reshuffle_iterator < 0:
        plt.xlabel(r'Age [yr]')
        plt.ylabel(r'Proportion of positive cultures')
        plt.title("This distribution is just an example. \nIt is not supported by any scientific finding.")
    plt.legend(prop={'size': 12})
    plt.tight_layout()
    plt.savefig(filename_nature)
    plt.close()
    print("Saved results as '{}'.".format(filename_nature))


if __name__ == '__main__':
    main()
