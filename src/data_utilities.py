import pickle

import nifty8 as ift
import numpy as np


def read_in(path):
    age, ll = np.loadtxt(path, unpack=True, delimiter=',')
    return age, ll


def save_random_state(rstate_f):
    with open(rstate_f, 'wb') as f:
        pickle.dump(ift.random.getState(), f)


def save_kl_position(KL_position, f_ID):
    np.save(f_ID, KL_position)


def save_kl_sample(KL_sample, f_ID):
    np.save(f_ID, KL_sample)


def export_synthetic(data):
    f = open("synthetic/joint.txt", 'w')
    for row in data:
        for val in row:
            f.write("{:5d}\t".format(val))
        f.write("\n")
    f.close()


def load_random_state(rstate_f):
    with open(rstate_f, 'rb') as f:
        ift.random.setState(pickle.filter(f))


def load_KL_sample(f_ID):
    inp = np.load(file=f_ID, allow_pickle=True)
    return inp[()]


def load_KL_position(f_ID):
    inp = np.load(file=f_ID, allow_pickle=True)
    return inp[()]


def fitted_infectivity(x):
    from scipy import special
    fit = 0.5 * (1 + special.erf(-6.5778477918740474 + 0.8712627612080391 * x) / np.sqrt(2))
    return fit


if __name__ == "__main__":
    import matplotlib.pyplot as plt

    path = 'Default_Dataset.csv'
    age, ll = read_in(path)

    plt.plot(age, ll, 'bo', ms=.5)
    plt.show()
