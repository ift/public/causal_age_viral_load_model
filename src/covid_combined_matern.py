#!/usr/bin/env python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2013-2024 Max-Planck-Society
# Author: Matteo Guardiani
#
# NIFTy is being developed at the Max-Planck-Institut fuer Astrophysik.

import argparse
import json
import os

import nifty8 as ift

from const import npix_age, npix_ll
from covid_matern_model import MaternCausalModel
from data import Data, InvertedData

# Parser Setup
parser = argparse.ArgumentParser()
parser.add_argument('--json_file', type=str, required=True, help="File (.json) in which the priors are configured.")
parser.add_argument('--csv_file', type=str, required=True,
                    help="File (.csv) in which the age and viral load data is stored.")
parser.add_argument('--reshuffle_seed', type=int, required=True,
                    help="Integer representing the seed for data reshuffling.")
args = parser.parse_args()


def create_output_directory(config_file_path, data_file_path, reshuffling_seed):
    # Results Output Folders
    json_filename = os.path.basename(config_file_path)
    csv_filename = os.path.basename(data_file_path)
    output_directory_path = os.path.join('../covid_viral_load_results', os.path.splitext(json_filename)[0],
                                         os.path.splitext(csv_filename)[0], str(reshuffling_seed))
    output_directory_path = os.path.normpath(output_directory_path)
    return output_directory_path


def perform_inference(config_path, data_path, reshuffling_seed, n_iterations, n_samples, geoVI=False):
    """ Variational Inference setup file

    FIXME: Add descriptor

    Required arguments:
    ----------

    config_path : str
        Path to the hyperparameter (prior) configuration file of the model.

    data_path : str
        Path to the file where the data is stored.

    reshuffling_seed : int
        seed used for data reshuffling.
        If seed = 0 then no reshuffling is performed
        If seed < 0  then the data axes are inverted
        If seed = -3141592 then the data axes are inverted and no reshuffling is performed.

    N_iterations : int or callable
        Number of minimization iterations. See ift.optimize_KL for more info.

    N_samples : int or callable
        Number of samples of posterior distribution. See ift.optimize_KL for more info.

    geoVI : Bool
        If true, performs geometric Variational Inference. Default is False


    Outputs:
    ----------
        Outputs of ift.optimize_kl into the customized output directory

    """

    # Read in the configuration file
    current_path = os.path.abspath('..')
    with open(config_path, "r") as file:
        setup = json.load(file)

    # Create the directory to which output the minimization results
    output_directory_path = create_output_directory(config_path, data_path, reshuffling_seed)

    # Prepare the dataset
    data = Data(npix_age, npix_ll, setup['threshold'], abs(reshuffling_seed), data_path)
    if reshuffling_seed < 0:
        if reshuffling_seed == -3141592:
            data = Data(npix_age, npix_ll, setup['threshold'], 0, data_path)
        data = InvertedData(data)

    # Load the model
    model = MaternCausalModel(setup, data)

    # Setup the response & define the amplitudes
    R = ift.GeometryRemover(model.lambda_combined.target)
    R_lamb = R(model.lambda_combined)

    # Collect the power spectrum amplitudes
    A1 = model.amplitudes[0]
    A2 = model.amplitudes[1]

    # Specify data space
    data_space = R_lamb.target

    # Generate mock signal and data
    seed = setup['seed']
    ift.random.push_sseq_from_seed(seed)
    data_field = ift.makeField(data_space, data.data)

    # Minimization parameters
    ic_sampling = ift.AbsDeltaEnergyController(deltaE=1e-5, iteration_limit=250, convergence_level=250)
    ic_newton = ift.AbsDeltaEnergyController(deltaE=1e-6, iteration_limit=25, name='newton', convergence_level=30)
    ic_sampling_nl_soft = ift.AbsDeltaEnergyController(name='Sampling (nonlin)', deltaE=1e-2, iteration_limit=10,
                                                       convergence_level=3)
    ic_sampling_nl_hard = ift.AbsDeltaEnergyController(name='Sampling (nonlin)', deltaE=1e-4, iteration_limit=25,
                                                       convergence_level=3)

    ic_sampling.enable_logging()
    ic_newton.enable_logging()
    minimizer = ift.NewtonCG(ic_newton, enable_logging=True)
    minimizer_sampling = lambda i_iter: ift.NewtonCG(ic_sampling_nl_soft) if i_iter < 17 else ift.NewtonCG(
        ic_sampling_nl_hard)

    # Set up likelihood
    likelihood = ift.PoissonianEnergy(data_field) @ R_lamb

    # Dictionary of operators to plot during minimization
    operators_to_plot = {"Reconstruction": model.lambda_combined,
                         "Conditional Probability Reconstruction": model.conditional_probability, "Power 1": A1,
                         "Power 2": A2}

    if geoVI:
        # geoVI
        ift.optimize_kl(likelihood, n_iterations, n_samples, minimizer, ic_sampling, minimizer_sampling,
                        output_directory=output_directory_path, plottable_operators=operators_to_plot)

    else:
        # MGVI
        ift.optimize_kl(likelihood, n_iterations, n_samples, minimizer, ic_sampling, None,
                        output_directory=output_directory_path, plottable_operators=operators_to_plot)

        # if  json_file:  #     shutil.copy(json_file, results_path)


if __name__ == '__main__':
    # Set up inference parameters
    N_iterations = 18
    N_samples = lambda i_iter: 20 if i_iter < N_iterations else 500
    geovi = True

    perform_inference(args.json_file, args.csv_file, args.reshuffle_seed, N_iterations, N_samples, geovi)
