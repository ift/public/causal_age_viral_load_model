#!/usr/bin/env python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2013-2024 Max-Planck-Society
# Author: Matteo Guardiani
#
# NIFTy is being developed at the Max-Planck-Institut fuer Astrophysik.

import argparse
import json
import os

import nifty8 as ift
import numpy as np

from const import npix_age, npix_ll
from covid_matern_model import MaternCausalModel
from data import Data, InvertedData

# Parser Setup
parser = argparse.ArgumentParser()
parser.add_argument('--json_file', type=str, required=True, help="File (.json) in which the priors are configured.")
parser.add_argument('--csv_file', type=str, required=True,
                    help="File (.csv) in which the age and viral load data is stored.")
parser.add_argument('--reshuffle_seed', type=int, required=True,
                    help="Integer representing the seed for data reshuffling.")

args = parser.parse_args()

json_file = args.json_file
csv_file = args.csv_file
reshuffle_iterator = args.reshuffle_seed


def fmt(x, pos):
    a, b = '{:.0e}'.format(x).split('e')
    b = int(b)
    return r'${} \times 10^{{{}}}$'.format(a, b)


def create_output_directory(config_file_path, data_file_path, reshuffling_seed):
    # Results Output Folders
    path_j = os.path.basename(config_file_path)
    path_t = os.path.basename(data_file_path)
    results_path = os.path.join('../covid_viral_load_results', os.path.splitext(path_j)[0], os.path.splitext(path_t)[0],
                                str(reshuffling_seed))
    results_path = os.path.normpath(results_path)

    save_results_path = os.path.join(results_path, 'results_essential/')
    if not os.path.exists(save_results_path):
        os.makedirs(save_results_path)

    evidence_path = os.path.join(save_results_path, 'evidence/')
    if not os.path.exists(evidence_path):
        os.makedirs(evidence_path)

    dataset = os.path.splitext(path_t)[0]
    elbo_file = "elbo_{}.txt".format(dataset + '_' + str(reshuffle_iterator))
    elbo_up_file = "elbo_up_{}.txt".format(dataset + '_' + str(reshuffle_iterator))
    elbo_lw_file = "elbo_low_{}.txt".format(dataset + '_' + str(reshuffle_iterator))

    if "indep" in json_file:
        elbo_file = "elbo_indep_{}.txt".format(dataset + '_' + str(reshuffle_iterator))
        elbo_up_file = "elbo_up_indep_{}.txt".format(dataset + '_' + str(reshuffle_iterator))
        elbo_lw_file = "elbo_low_indep_{}.txt".format(dataset + '_' + str(reshuffle_iterator))

    elbo_file, elbo_up_file, elbo_lw_file = [evidence_path + file for file in [elbo_file, elbo_up_file, elbo_lw_file]]
    return results_path, evidence_path, elbo_file, elbo_lw_file, elbo_up_file


if __name__ == '__main__':
    # Read in the configuration file
    current_path = os.path.abspath('..')
    file_setup = open(json_file, "r")
    setup = json.load(file_setup)
    file_setup.close()

    # Load output directory
    results_path, evidence_path, elbo_file, elbo_lw_file, elbo_up_file = create_output_directory(json_file, csv_file,
                                                                                                 reshuffle_iterator)

    # Prepare the dataset
    data = Data(npix_age, npix_ll, setup['threshold'], abs(reshuffle_iterator), csv_file)
    if reshuffle_iterator < 0:
        if reshuffle_iterator == -3141592:
            data = Data(npix_age, npix_ll, setup['threshold'], 0, csv_file)
        data = InvertedData(data)

    # Load the model
    model = MaternCausalModel(setup, data)

    # Setup the response & define the amplitudes
    R = ift.GeometryRemover(model.lambda_combined.target)
    R_lamb = R(model.lambda_combined)

    A1 = model.amplitudes[0]
    A2 = model.amplitudes[1]

    # Specify data space
    data_space = R_lamb.target

    samples = ift.ResidualSampleList.load(results_path + "/pickle/last")
    n_samples = samples.n_samples

    # Specify the data field
    data_space = R_lamb.target
    data_field = ift.makeField(data_space, data.data)

    # Specify the likelihood and information hamiltonian
    likelihood = ift.PoissonianEnergy(data_field) @ R_lamb
    hamiltonian = ift.StandardHamiltonian(likelihood)

    # Gather elbo results
    elbo_samples, stats = ift.estimate_evidence_lower_bound(hamiltonian, samples, 250, min_lh_eval=1e-4)

    # Save elbo stats to file
    np.savetxt(elbo_file, np.array([stats["elbo_mean"].val]))
    np.savetxt(elbo_up_file, np.array([stats["elbo_up"].val]))
    np.savetxt(elbo_lw_file, np.array([stats["elbo_lw"].val]))
    [print(f"Results saved as {file}\n") for file in [elbo_file, elbo_up_file, elbo_lw_file]]
